package ru.atconsulting.communications;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.atconsulting.entities.AtcContent;
import ru.atconsulting.entities.AtcIncident;
import ru.atconsulting.utils.AtcCredentials;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Roman.
 */

//Класс для создания GET запроса к веб-сервису HPSM.
@Component
public final class GetRequestFactory {
    private static final Logger log = LoggerFactory.getLogger(GetRequestFactory.class);

    private static ObjectMapper mapper = new ObjectMapper();
    private static RestTemplate restTemplate = new RestTemplate();

    private URI uri;
    private ResponseEntity<String> response;
    //private HashMap<String, String> otherParameters; //TODO: Возможно, будет необходимо реализовать возможность добавлять параметры

    private int start = -1;
    private int count = -1;
    private boolean expand;

    private GetRequestFactory(URI restServiceURI, int start, int count, boolean isExpand) {
        this.uri = restServiceURI;
        this.start = start;
        this.count = count;
        this.expand = isExpand;
    }

    public static GetRequestFactory create(String uri, int start, int count, boolean expand) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(uri)
                .queryParam("start", start)
                .queryParam("count", count);
        if (expand) {
            uriComponentsBuilder.queryParam("view", "expand");
        }
        log.info("[AT-Consulting] Building GET request to " + uriComponentsBuilder.build().encode().toString());
        return new GetRequestFactory(uriComponentsBuilder.build().encode().toUri(), start, count, expand);
    }

    public void sendRequest() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + AtcCredentials.getCredentials());

        response = restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(httpHeaders), String.class);
    }

    public ResponseEntity<String> getResponse() {
        return response;
    }

    public List<AtcIncident> getIncidents() throws IOException {
        List<AtcIncident> incidents = new ArrayList<>();

        List<AtcContent> content = mapper.readValue(
                getContentNode(response).toString(),
                new TypeReference<List<AtcContent>>(){}
                );

        for (AtcContent incident :content) {
            incidents.add(incident.getIncident());
        }

        return incidents;
    }

    private JsonNode getContentNode(ResponseEntity<String> responseEntity) throws IOException {
        JsonNode root = mapper.readTree(response.getBody());
        JsonNode node = root.path("content");
        log.info("[AT-Consulting] Parsing response from "+ uri + " with HttpStatus: " + response.getStatusCode().name());

        return node;
    }
}
