package ru.atconsulting.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Roman
 */

public class AtcContent {
    private AtcIncident Incident;

    public AtcIncident getIncident() {
        return Incident;
    }

    @JsonProperty("Incident")
    public void setIncident(AtcIncident incident) {
        Incident = incident;
    }

    @Override
    public String toString() {
        return Incident.toString();
    }
}
