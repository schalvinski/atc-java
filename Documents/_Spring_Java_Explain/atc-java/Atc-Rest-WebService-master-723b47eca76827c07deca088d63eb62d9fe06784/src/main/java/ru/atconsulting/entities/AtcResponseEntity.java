package ru.atconsulting.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.Collection;


/**
 * @author Roman.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AtcResponseEntity {

    public AtcResponseEntity(){
    }

    private Integer count;
    private Integer start;
    private Integer totalCount;
    private String[] messages;
    private String resourceName;
    private Integer returnCode;
    private Collection<AtcIncident> incidents;

    // <editor-fold defaultstate="collapsed" desc="[Setters]">
    @JsonProperty("@count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("@start")
    public void setStart(Integer start) {
        this.start = start;
    }

    @JsonProperty("@totalcount")
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @JsonProperty("Messages")
    public void setMessages(String[] messages) {
        this.messages = messages;
    }

    @JsonProperty("ResourceName")
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    @JsonProperty("ReturnCode")
    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("content")
    public void setContent(Collection<AtcIncident> content) {
        this.incidents = content;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="[Getters]">

    public Integer getCount() {
        return count;
    }

    public Integer getStart() {
        return start;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public String[] getMessages() {
        return messages;
    }

    public String getResourceName() {
        return resourceName;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public Collection<AtcIncident> getContent() {
        return incidents;
    }

    // </editor-fold>


    @Override
    public String toString() {
        return "RestRequestEntity{" +
                "count='" + count + '\'' +
                ", start='" + start + '\'' +
                ", totalCount='" + totalCount + '\'' +
                ", messages=" + Arrays.toString(messages) +
                ", resourceName='" + resourceName + '\'' +
                ", returnCode='" + returnCode + '\'' +
                "\n" + "Content:" + incidents.toString();
    }

}
