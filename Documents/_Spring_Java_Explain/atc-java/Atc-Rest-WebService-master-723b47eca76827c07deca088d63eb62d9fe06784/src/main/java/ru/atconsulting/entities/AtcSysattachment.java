package ru.atconsulting.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author Roman.
 */

@JsonRootName("Sysattachment")
public class AtcSysattachment {

    private String application;
    private String compressed;
    private String compressedSize;
    private String fileName;
    private String mimeType;
    private String segment;
    private String size;
    private String topic;
    private String type;
    private String uid;
    private String sysmodcount;
    private String sysmodtime;
    private String sysmoduser;
    private String uploadBy;
    private String uploadDate;

    // <editor-fold defaultstate="collapsed" desc="[Setters]"

    @JsonProperty("Application")
    public void setApplication(String application) {
        this.application = application;
    }

    @JsonProperty("Compressed")
    public void setCompressed(String compressed) {
        this.compressed = compressed;
    }

    @JsonProperty("Compressed_Size")
    public void setCompressedSize(String compressedSize) {
        this.compressedSize = compressedSize;
    }

    @JsonProperty("File_Name")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @JsonProperty("MIME_Type")
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @JsonProperty("Segment")
    public void setSegment(String segment) {
        this.segment = segment;
    }

    @JsonProperty("Size")
    public void setSize(String size) {
        this.size = size;
    }

    @JsonProperty("Topic")
    public void setTopic(String topic) {
        this.topic = topic;
    }

    @JsonProperty("Type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("uid")
    public void setUid(String uid) {
        this.uid = uid;
    }

    @JsonProperty("sysmodcount")
    public void setSysmodcount(String sysmodcount) {
        this.sysmodcount = sysmodcount;
    }

    @JsonProperty("sysmodtime")
    public void setSysmodtime(String sysmodtime) {
        this.sysmodtime = sysmodtime;
    }

    @JsonProperty("sysmoduser")
    public void setSysmoduser(String sysmoduser) {
        this.sysmoduser = sysmoduser;
    }

    @JsonProperty("upload.by")
    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    @JsonProperty("upload.date")
    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="[Getters]"

    public String getApplication() {
        return application;
    }

    public String getCompressed() {
        return compressed;
    }

    public String getCompressedSize() {
        return compressedSize;
    }

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getSegment() {
        return segment;
    }

    public String getSize() {
        return size;
    }

    public String getTopic() {
        return topic;
    }

    public String getType() {
        return type;
    }

    public String getUid() {
        return uid;
    }

    public String getSysmodcount() {
        return sysmodcount;
    }

    public String getSysmodtime() {
        return sysmodtime;
    }

    public String getSysmoduser() {
        return sysmoduser;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    // </editor-fold>

    @Override
    public String toString() {
        return "Sysattachment{" +
                "application='" + application + '\'' +
                ", compressed='" + compressed + '\'' +
                ", compressedSize='" + compressedSize + '\'' +
                ", fileName='" + fileName + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", segment='" + segment + '\'' +
                ", size='" + size + '\'' +
                ", topic='" + topic + '\'' +
                ", type='" + type + '\'' +
                ", uid='" + uid + '\'' +
                ", sysmodcount='" + sysmodcount + '\'' +
                ", sysmodtime='" + sysmodtime + '\'' +
                ", sysmoduser='" + sysmoduser + '\'' +
                ", uploadBy='" + uploadBy + '\'' +
                ", uploadDate='" + uploadDate + '\'' +
                '}';
    }
}