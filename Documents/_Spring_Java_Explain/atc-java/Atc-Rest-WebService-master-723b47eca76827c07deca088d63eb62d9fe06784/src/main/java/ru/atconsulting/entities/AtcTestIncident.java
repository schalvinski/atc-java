package ru.atconsulting.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author Roman
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName(value = "atcTestIncidents")
public class AtcTestIncident {
    private String area;
    private String assignee;
    private String assignmentGroup;
    private String category;
    private String closureCode;
    private String company;
    private String contact;
    private String[] description;
    private String developerNotes;
    private String impact;
    private String number;
    private String location;
    private String openTime;
    private String priority;
    private String privateInformation;
    private String problemType;
    private String resolvedTime;
    private String service;
    private String[] solution;
    private String status;
    private String subArea;
    private String ticketOwner;
    private String title;
    private String updatedBy;
    private String updatedTime;
    private String urgency;
    private String firstLvReportDone;


    // <editor-fold defaultstate="collapsed" desc="[Setters]">
    @JsonProperty("Area")
    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("Assignee")
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    @JsonProperty("AssignmentGroup")
    public void setAssignmentGroup(String assignmentGroup) {
        this.assignmentGroup = assignmentGroup;
    }

    @JsonProperty("Category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("ClosureCode")
    public void setClosureCode(String closureCode) {
        this.closureCode = closureCode;
    }

    @JsonProperty("Company")
    public void setCompany(String company) {
        this.company = company;
    }

    @JsonProperty("Contact")
    public void setContact(String contact) {
        this.contact = contact;
    }

    @JsonProperty("Description")
    public void setDescription(String[] description) {
        this.description = description;
    }

    @JsonProperty("DeveloperNotes")
    public void setDeveloperNotes(String developerNotes) {
        this.developerNotes = developerNotes;
    }

    @JsonProperty("Impact")
    public void setImpact(String impact) {
        this.impact = impact;
    }

    @JsonProperty("Number")
    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("Location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("OpenTime")
    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    @JsonProperty("Priority")
    public void setPriority(String priority) {
        this.priority = priority;
    }

    @JsonProperty("PrivateInformation")
    public void setPrivateInformation(String privateInformation) {
        this.privateInformation = privateInformation;
    }

    @JsonProperty("ProblemType")
    public void setProblemType(String problemType) {
        this.problemType = problemType;
    }

    @JsonProperty("ResolvedTime")
    public void setResolvedTime(String resolvedTime) {
        this.resolvedTime = resolvedTime;
    }

    @JsonProperty("Service")
    public void setService(String service) {
        this.service = service;
    }

    @JsonProperty("Solution")
    public void setSolution(String[] solution) {
        this.solution = solution;
    }

    @JsonProperty("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("Subarea")
    public void setSubArea(String subArea) {
        this.subArea = subArea;
    }

    @JsonProperty("TicketOwner")
    public void setTicketOwner(String ticketOwner) {
        this.ticketOwner = ticketOwner;
    }

    @JsonProperty("Title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("UpdatedBy")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @JsonProperty("UpdatedTime")
    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    @JsonProperty("Urgency")
    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }

    @JsonProperty("firstLvReportDone")
    public void setFirstLvReportDone(String firstLvReportDone) {
        this.firstLvReportDone = firstLvReportDone;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="[Getters]">

    public String getArea() {
        return area;
    }

    public String getAssignee() {
        return assignee;
    }

    public String getAssignmentGroup() {
        return assignmentGroup;
    }

    public String getCategory() {
        return category;
    }

    public String getClosureCode() {
        return closureCode;
    }

    public String getCompany() {
        return company;
    }

    public String getContact() {
        return contact;
    }

    public String[] getDescription() {
        return description;
    }

    public String getDeveloperNotes() {
        return developerNotes;
    }

    public String getImpact() {
        return impact;
    }

    public String getNumber() {
        return number;
    }

    public String getLocation() {
        return location;
    }

    public String getOpenTime() {
        return openTime;
    }

    public String getPriority() {
        return priority;
    }

    public String getPrivateInformation() {
        return privateInformation;
    }

    public String getProblemType() {
        return problemType;
    }

    public String getResolvedTime() {
        return resolvedTime;
    }

    public String getService() {
        return service;
    }

    public String[] getSolution() {
        return solution;
    }

    public String getStatus() {
        return status;
    }

    public String getSubArea() {
        return subArea;
    }

    public String getTicketOwner() {
        return ticketOwner;
    }

    public String getTitle() {
        return title;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public String getUrgency() {
        return urgency;
    }

    public String getFirstLvReportDone() {
        return firstLvReportDone;
    }

    // </editor-fold>

}

