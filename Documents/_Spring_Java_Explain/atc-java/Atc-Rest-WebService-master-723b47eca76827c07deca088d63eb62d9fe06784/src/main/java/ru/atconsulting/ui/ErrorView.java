package ru.atconsulting.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;

/**
 * @author Roman
 */
@SpringView(name = ErrorView.VIEW_NAME)
public class ErrorView extends ErrorViewDesign implements View {
    public static final String VIEW_NAME = "error";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        errorLabel.setValue("Error. " + event.getViewName() + " couldn't be found");
    }
}
