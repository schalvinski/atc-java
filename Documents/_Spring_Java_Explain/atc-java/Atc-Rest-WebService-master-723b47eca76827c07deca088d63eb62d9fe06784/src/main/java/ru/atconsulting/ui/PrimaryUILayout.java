package ru.atconsulting.ui;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Roman.
 */

public class PrimaryUILayout extends PrimaryUILayoutDesign implements ViewDisplay {
    private static final String STYLE_SELECTED = "selected";
    private Navigator navigator;

    private ArrayList<String> registeredViews = new ArrayList<>();

    public PrimaryUILayout() {
        registeredViews.add(DashboardView.VIEW_NAME);
        registeredViews.add(SchedulerView.VIEW_NAME);
        registeredViews.add(SettingsView.VIEW_NAME);

        navigator = new Navigator(UI.getCurrent(), (ViewDisplay) this);
        navigator.setErrorView(ErrorView.class);

        addNavigatorView(DashboardView.VIEW_NAME, DashboardView.class, menuButton1);
        addNavigatorView(MainView.VIEW_NAME, MainView.class, menuButton2);
        addNavigatorView(SchedulerView.VIEW_NAME, SchedulerView.class, menuButton3);
        addNavigatorView(SettingsView.VIEW_NAME, SettingsView.class, menuButton4);

        if (navigator.getState().isEmpty()) {
            navigator.navigateTo(DashboardView.VIEW_NAME);
        }
    }

    private void doNavigate(String viewName) {
            getUI().getNavigator().navigateTo(viewName);
    }

    private void addNavigatorView(String viewName,
                                  Class<? extends View> viewClass, Button menuButton) {
        navigator.addView(viewName, viewClass);
        menuButton.addClickListener(event -> doNavigate(viewName));
        menuButton.setData(viewClass.getName());
    }


    private void adjustStyleByData(Component component, Object data) {
        if (component instanceof Button) {
            if (data != null && data.equals(((Button) component).getData())) {
                component.addStyleName(STYLE_SELECTED);
            } else {
                component.removeStyleName(STYLE_SELECTED);
            }
        }
    }

    @Override
    public void showView(View view) {
        if (view instanceof Component) {
            scroll_panel.setContent((Component) view);
            Iterator<Component> it = side_bar.iterator();
            while (it.hasNext()) {
                adjustStyleByData(it.next(), view.getClass().getName());
            }
        } else {
            throw new IllegalArgumentException("View is not a Component");
        }
    }
}
