package ru.atconsulting.communications;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.atconsulting.utils.AtcCredentials;

import java.net.URI;
import java.util.Collections;

/**
 * @author Roman.
 */

public class AttachmentTreatment {

    private static final Logger log = LoggerFactory.getLogger(AttachmentTreatment.class);

    private static RestTemplate restTemplate = new RestTemplate();
    private static HttpHeaders httpHeaders = new HttpHeaders();
    private static ObjectMapper mapper = new ObjectMapper();

    static {
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
        httpHeaders.add("Authorization", "Basic " + AtcCredentials.getCredentials());
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    }

    private URI uri;
    private String uid;
    private String cid;
    private byte[] contents;

    private AttachmentTreatment(URI uri, String uid, String cid, byte[] contents){
        this.uri = uri;
        this.uid = uid;
        this.cid = cid;
        this.contents = contents;
    }

    public static AttachmentTreatment createFromUri(String uri, String uid, String cid){
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(uri)
                .pathSegment(uid.toLowerCase())
                .pathSegment("attachments")
                .pathSegment("cid:" + cid);
        uri = uriComponentsBuilder.build().toString();
        log.info("[AT-Consulting] Attempting to download file from " + uri);
        ResponseEntity<byte[]> response = restTemplate.exchange(
                uri,
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders),
                byte[].class
        );
        return new AttachmentTreatment(
                uriComponentsBuilder.build().encode().toUri(),
                uid,
                cid,
                response.getBody()
        );
    }

    public JsonNode uploadTo(String uri, String uid, MediaType mediaType, String filename) throws Exception {
        httpHeaders.add("Content-Disposition","attachments; filename="+ filename);
        httpHeaders.setContentType(mediaType);

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(uri)
                .pathSegment(uid)
                .pathSegment("attachments");

        HttpEntity request = new HttpEntity<>(contents, httpHeaders);
        log.info("[AT-Consulting] Attempting to upload file to " + uri);
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uriComponentsBuilder.build().toString(),
                HttpMethod.POST,
                request,
                String.class);

        return mapper.readTree(responseEntity.getBody());
    }

    public byte[] getContents() throws Exception{
        return contents;
    }
}


