package ru.atconsulting.communications;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.atconsulting.entities.AtcIncident;
import ru.atconsulting.utils.AtcCredentials;

import java.nio.charset.Charset;

/**
 * @author Roman.
 */

@Component
public class PostRequestFactory {
    private static final Logger log = LoggerFactory.getLogger(PostRequestFactory.class);

    private static ObjectMapper mapper = new ObjectMapper();
    private static RestTemplate restTemplate = new RestTemplate();
    private static HttpHeaders httpHeaders = new HttpHeaders();

    public static String sendIncident(String uri, AtcIncident incident) {
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        restTemplate.setErrorHandler(new CustomResponseErrorHandler());

        httpHeaders.add("Authorization", "Basic " + AtcCredentials.getCredentials());
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

        String responseBody;
        try {
            log.info("[AT-Consulting] Serializing Incident into Json and building request entity");
            HttpEntity<String> request = new HttpEntity<>(
                    mapper.writeValueAsString(incident),
                    httpHeaders
            );

            log.info("[AT-Consulting] Sending POST request to " + uri);
            ResponseEntity<String> response = restTemplate.exchange(
                    uri,
                    HttpMethod.POST,
                    request,
                    String.class
            );
            log.info("[AT-Consulting] The request was sent with status " + response.getStatusCode());
            responseBody = response.getBody();

        } catch (JsonProcessingException | HttpClientErrorException hcex) {
            log.error("[AT-Consulting] " + hcex.toString());
            responseBody = hcex.getMessage();
        }

        return responseBody;
    }
}