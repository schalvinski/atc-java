package ru.atconsulting.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;

/**
 * @author Roman.
 */

@SpringView(name = SchedulerView.VIEW_NAME)
public class DashboardView extends DashboardViewDesign implements View {
    public static final String VIEW_NAME = "dashboard";

    @Override
    public void enter(ViewChangeEvent event) {
    }

}
