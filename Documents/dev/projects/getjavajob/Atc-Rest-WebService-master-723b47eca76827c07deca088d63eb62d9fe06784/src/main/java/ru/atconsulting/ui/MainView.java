package ru.atconsulting.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import ru.atconsulting.communications.GetRequestFactory;
import ru.atconsulting.communications.PostRequestFactory;
import ru.atconsulting.entities.AtcIncident;
import ru.atconsulting.utils.AtcUriResources;

import java.util.List;

/**
 * @author Roman.
 */

@SpringView(name = MainView.VIEW_NAME)
public class MainView extends MainViewDesign implements View {
    static final String VIEW_NAME = "main";

    private Panel sendRequestPanel = new Panel();
    private HorizontalLayout sendRequestPanelLayout = new HorizontalLayout();
    private FormLayout createIncidentLayout = new FormLayout();


    private Label createNewIncidentLbl = new Label("Create new incident:");
    private Label retrieveIncidentsLbl = new Label("Retrieve incidents:");

    private TextField startField = new TextField();
    private TextField countField = new TextField();

    private TextField areaField = new TextField("area:", "Прочее");
    private TextField assignmentGroupField = new TextField("assignmentGroup:", "03 (Бурятия).Поддержка внедрения");
    private TextField categoryField = new TextField("category:", "Инцидент");
    private TextField contactField = new TextField("contact:", "быстров федор геннадьевич");
    private TextField impactField = new TextField("impact:", "4");
    private TextField priorityField = new TextField("priority:", "3");
    private TextField problemTypeField = new TextField("problemType:", "Инцидент");
    private TextField serviceField = new TextField("service:", "АВК");
    private TextField statusField = new TextField("status", "Open");
    private TextField subAreaField = new TextField("subarea", "Прочее");
    private TextField titleField = new TextField("title");
    private TextField urgencyField = new TextField("urgency", "3");

    private Button createIncidentBtn = new Button("Create Incident");
    private Button sendRequestButton = new Button("Send Request");
    private Grid<AtcIncident> grid = new Grid<>(AtcIncident.class);

    private Integer start;
    private Integer count;
    private List<AtcIncident> incidents;


    public MainView() {
        createIncidentLayout.addStyleName("outlined");
        createIncidentLayout.setSizeFull();

        createNewIncidentLbl.addStyleName("h3");
        retrieveIncidentsLbl.addStyleName("h3");
        grid.setColumns("incidentID", "title", "openedBy", "company", "assignee", "resolvedTime");

        createIncidentLayout.addComponents(
                titleField,
                areaField,
                assignmentGroupField,
                categoryField,
                contactField,
                impactField,
                priorityField,
                problemTypeField,
                serviceField,
                statusField,
                subAreaField,
                urgencyField

        );

        createIncidentLayout.addComponent(createIncidentBtn);

        startField.setPlaceholder("start:");
        startField.setDescription("Add integer value to start parameter");
        countField.setPlaceholder("count:");
        countField.setDescription("Add integer value to count parameter");

        sendRequestPanelLayout.addComponents(
                startField,
                countField,
                sendRequestButton
        );

        sendRequestPanel.setContent(sendRequestPanelLayout);

        grid.setSizeFull();
        startField.setWidth("100px");
        countField.setWidth("100px");
        sendRequestPanel.setSizeUndefined();

        rootLayout.addComponents(
                createNewIncidentLbl,
                createIncidentLayout,
                retrieveIncidentsLbl,
                sendRequestPanel,
                grid
        );

        createIncidentBtn.addClickListener(e -> {
            AtcIncident newIncident = new AtcIncident();

            newIncident.setArea(areaField.getValue());
            newIncident.setAssignmentGroup(assignmentGroupField.getValue());
            newIncident.setCategory(categoryField.getValue());
            newIncident.setContact(contactField.getValue());
            newIncident.setImpact(impactField.getValue());
            newIncident.setPriority(priorityField.getValue());
            newIncident.setProblemType(problemTypeField.getValue());
            newIncident.setService(serviceField.getValue());
            newIncident.setStatus(statusField.getValue());
            newIncident.setSubArea(subAreaField.getValue());
            newIncident.setTitle(titleField.getValue());
            newIncident.setUrgency(urgencyField.getValue());
            newIncident.setDescription(new String[]{"null"});

            String node = PostRequestFactory.sendIncident(
                    AtcUriResources.INCIDENTS_URI,
                    newIncident
            );

            Notification.show(node, Notification.Type.TRAY_NOTIFICATION);
        });

        sendRequestButton.addClickListener(e -> {
            try {
                this.start = Integer.parseInt(startField.getValue());
                this.count = Integer.parseInt(countField.getValue());

                GetRequestFactory incidentsRequest = GetRequestFactory
                        .create(AtcUriResources.INCIDENTS_URI,
                                this.start,
                                this.count,
                                true
                        );

                incidentsRequest.sendRequest();
                this.incidents = incidentsRequest.getIncidents();
                grid.setItems(incidents);

            } catch (NumberFormatException nfex) {
                Notification.show(
                        "Please add correct values to start and count fields",
                        Notification.Type.TRAY_NOTIFICATION
                );
            } catch (Exception ex) {
                Notification.show(
                        ex.toString(),
                        Notification.Type.ERROR_MESSAGE
                );
            }
        });
    }

    @Override
    public void enter(ViewChangeEvent event) {
    }
}
