package ru.atconsulting.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

/**
 * @author Roman.
 */

@Theme("mytheme")
@SpringUI
@SpringViewDisplay
public class PrimaryUI extends UI implements ViewDisplay {
    private Panel springViewDisplay;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setContent(new PrimaryUILayout());
    }

    @Override
    public void showView(View view) {
        springViewDisplay.setContent((Component) view);
    }
}