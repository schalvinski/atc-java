package ru.atconsulting.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;

/**
 * @author Roman
 */

@SpringView(name = SchedulerView.VIEW_NAME)
public class SchedulerView  extends SchedulerViewDesign implements View {
    public static final String VIEW_NAME = "scheduler";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}
