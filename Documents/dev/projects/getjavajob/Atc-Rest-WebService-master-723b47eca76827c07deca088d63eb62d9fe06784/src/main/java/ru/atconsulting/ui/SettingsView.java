package ru.atconsulting.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;

/**
 * @author Roman.
 */

@SpringView(name = SettingsView.VIEW_NAME)
public class SettingsView extends SettingsViewDesign implements View {
    public static final String VIEW_NAME = "settings";

    @Override
    public void enter(ViewChangeEvent event) {
    }
}
