package ru.atconsulting.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Roman
 */

//Singleton for providing Authentication information, thread-safe.
@Component
public final class AtcCredentials {
    private static final Logger log = LoggerFactory.getLogger(AtcCredentials.class);
    private static volatile String credential;

    public static String getCredentials() {
        String localCredential = credential;
        if (localCredential == null) {
            synchronized (AtcCredentials.class) {
                localCredential = credential;
                if (localCredential == null) {
                    try {
                        BufferedReader reader = new BufferedReader(new FileReader("./src/main/resources/auth.txt"));
                        credential = localCredential = reader.readLine();
                        reader.close();
                        log.info("[AT-Consulting] Authentication file read successfully");
                    } catch (FileNotFoundException fnfex){
                        log.info("[AT-Consulting] Authentication file not found");
                    } catch (IOException ioex){
                        log.info("[AT-Consulting] Problems with reading Authentication file");
                    }
                }
            }
        }
        return localCredential;
    }
}
