package ru.atconsulting.utils;

/**
 * @author Roman
 */

public class AtcUriResources {
    public static final String INCIDENTS_URI = "http://192.168.14.162:13080/SM/9/rest/incidents/";

    public static final String TESTINCIDENTS_URI = "http://192.168.14.162:13080/SM/9/rest/atcTestIncidents/";

    public static final String SYSATTACHMENTS_URI = "http://192.168.14.162:13080/SM/9/rest/sysattachments";

}
