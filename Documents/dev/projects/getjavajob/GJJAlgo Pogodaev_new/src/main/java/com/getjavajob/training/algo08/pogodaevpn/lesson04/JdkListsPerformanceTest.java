package com.getjavajob.training.algo08.pogodaevpn.lesson04;

import com.getjavajob.training.algo08.pogodaevpn.util.StopWatch;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by paul on 11.06.16.
 */
public class JdkListsPerformanceTest {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < 10000000; i++) {
            arrayList.add(i);
        }

        LinkedList<Number> numbers = new LinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            numbers.add(i);
        }

        System.out.println("Addition/remove to/from the beginning test");
        System.out.println("==============================");
        StopWatch.start();
        arrayList.add(0, 147);
        System.out.println("ArrayList.add(e): " + StopWatch.getElapsedTime());
        StopWatch.start();
        numbers.add(0, 147);
        System.out.println("LinkedList.add(e): " + StopWatch.getElapsedTime());
        System.out.println("------------");
        StopWatch.start();
        arrayList.remove(0);
        System.out.println("ArrayList.remove(e): " + StopWatch.getElapsedTime());
        StopWatch.start();
        numbers.remove(0);
        System.out.println("LinkedList.remove(e): " + StopWatch.getElapsedTime());
        System.out.println("------------\n");

        System.out.println("Addition/remove to/from the middle test");
        System.out.println("==============================");
        StopWatch.start();
        arrayList.add(5000000, 147);
        System.out.println("ArrayList.add(e): " + StopWatch.getElapsedTime());
        StopWatch.start();
        numbers.add(5000000, 147);
        System.out.println("LinkedList.add(e): " + StopWatch.getElapsedTime());
        System.out.println("------------");
        StopWatch.start();
        arrayList.remove(5000000);
        System.out.println("ArrayList.remove(e): " + StopWatch.getElapsedTime());
        StopWatch.start();
        numbers.remove(5000000);
        System.out.println("LinkedList.remove(e): " + StopWatch.getElapsedTime());
        System.out.println("------------");

        System.out.println("Addition/remove to/from the end test");
        System.out.println("==============================");
        StopWatch.start();
        arrayList.add(arrayList.size() - 1, 147);
        System.out.println("ArrayList.add(e): " + StopWatch.getElapsedTime());
        StopWatch.start();
        numbers.add(numbers.size() - 1, 147);
        System.out.println("LinkedList.add(e): " + StopWatch.getElapsedTime());
        System.out.println("------------");
        StopWatch.start();
        arrayList.remove(arrayList.size() - 1);
        System.out.println("ArrayList.remove(e): " + StopWatch.getElapsedTime());
        StopWatch.start();
        numbers.remove(numbers.size() - 1);
        System.out.println("LinkedList.remove(e): " + StopWatch.getElapsedTime());
        System.out.println("------------");
    }
}
