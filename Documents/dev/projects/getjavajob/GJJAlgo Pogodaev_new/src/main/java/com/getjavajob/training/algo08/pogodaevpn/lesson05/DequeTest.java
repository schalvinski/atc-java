package com.getjavajob.training.algo08.pogodaevpn.lesson05;

import com.getjavajob.training.algo08.pogodaevpn.util.Assert;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.pogodaevpn.util.Assert.assertEquals;

/**
 * Created by paul on 23.05.16.
 */
public class DequeTest {
    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testGetFirst();
        testGetLast();
        testElement();
        testPeekFirst();
        testPeekLast();
        testPollFirst();
        testPollLast();
        testPopPush();
        testRemove();
        testRemoveLast();
        testRemoveLastOccurrence();
    }

    private static void testRemoveLastOccurrence() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(12);
        arrayDeque.add(13);
        Assert.assertEquals("DequeTest.testRemoveLastOccurrence", true, arrayDeque.removeLastOccurrence(12));
    }

    private static void testRemoveLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        Assert.assertEquals("DequeTest.testRemoveLast", 13, arrayDeque.removeLast());
        String msg = "NoSuchElementException";
        try {
            arrayDeque.removeLast();
            Assert.fail(msg);
        } catch (Exception e) {
            Assert.assertEquals("DequeTest.testRemoveLastException", msg, e.getClass().getSimpleName());
        }
    }

    private static void testRemove() {
        Deque<Number> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        Assert.assertEquals("DequeTest.testRemove", true, arrayDeque.remove(13));

        String msg = "NoSuchElementException";
        try {
            arrayDeque.remove();
            Assert.fail(msg);
        } catch (Exception e) {
            Assert.assertEquals("DequeTest.testRemoveException", msg, e.getClass().getSimpleName());
        }
    }

    //the same as addFirstMethod
    private static void testPopPush() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.push(12);
        Assert.assertEquals("DequeTest.testPopPush", 12, arrayDeque.pop());
    }

    private static void testPollLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        Assert.assertEquals("DequeTest.testPollLast", 12, arrayDeque.pollLast());
        Assert.assertEquals("DequeTest.testPollLastSizeAfterPoll", 1, arrayDeque.size());
    }

    private static void testPollFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        Assert.assertEquals("DequeTest.testPollFirst", 13, arrayDeque.pollFirst());
        Assert.assertEquals("DequeTest.testPollFirstSizeAfterPoll", 1, arrayDeque.size());
    }

    private static void testPeekLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        Assert.assertEquals("DequeTest.testPeekLast", 12, arrayDeque.peekLast());
    }

    private static void testPeekFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        Assert.assertEquals("DequeTest.testPeekFirst", 13, arrayDeque.peekFirst());
    }

    //the same as addFirstMethod
    private static void testElement() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.add(12);
        Assert.assertEquals("DequeTest.testElement", 13, arrayDeque.element());
    }

    private static void testGetLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(12);
        Assert.assertEquals("DequeTest.testGetLast", 12, arrayDeque.getLast());
        arrayDeque.remove();
        String msg = "NoSuchElementException";
        try {
            arrayDeque.getLast();
            Assert.fail(msg);
        } catch (NoSuchElementException e) {
            Assert.assertEquals("DequeTest.testGetLastException", msg, e.getClass().getSimpleName());
        }
    }

    private static void testGetFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        Assert.assertEquals("DequeTest.testGetFirst", 13, arrayDeque.getFirst());
        arrayDeque.remove();
        String msg = "NoSuchElementException";
        try {
            arrayDeque.getFirst();
            Assert.fail(msg);
        } catch (Exception e) {
            Assert.assertEquals("DequeTest.testGetFirstException", msg, e.getClass().getSimpleName());
        }
    }

    public static void testAddFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.addFirst(12);
        Assert.assertEquals("DequeTest.testAddFirst", 12, arrayDeque.getFirst());

        String msg = "NullPointerException";
        try {
            arrayDeque.addFirst(null);
            Assert.fail(msg);
        } catch (Exception e) {
            Assert.assertEquals("DequeTest.testAddFirstException", msg, e.getClass().getSimpleName());
        }
    }

    public static void testAddLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.add(13);
        arrayDeque.addLast(12);
        Assert.assertEquals("DequeTest.testAddLast", 12, arrayDeque.getLast());

        String msg = "NullPointerException";
        try {
            arrayDeque.addLast(null);
            Assert.fail(msg);
        } catch (Exception e) {
            Assert.assertEquals("DequeTest.testAddLastException", msg, e.getClass().getSimpleName());
        }
    }

    //the same as addFirst
    public static void testOfferFirst() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.offer(15);
        arrayDeque.offerFirst(14);
        Assert.assertEquals("DequeTest.testOfferFirst", 14, arrayDeque.getFirst());
        Assert.assertEquals("DequeTest.testOfferFirstBool", true, arrayDeque.offerFirst(1));
    }

    public static void testOfferLast() {
        Deque<Integer> arrayDeque = new ArrayDeque();
        arrayDeque.offer(15);
        arrayDeque.offerLast(14);
        Assert.assertEquals("DequeTest.testOfferLast", 14, arrayDeque.getLast());
        Assert.assertEquals("DequeTest.testOfferLastBool", true, arrayDeque.offerLast(1));
    }
}

