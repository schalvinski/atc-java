package com.getjavajob.training.algo08.pogodaevpn.lesson06;

import com.getjavajob.training.algo08.pogodaevpn.util.Assert;

import java.util.LinkedHashSet;

import static com.getjavajob.training.algo08.pogodaevpn.util.Assert.assertEquals;

/**
 * Created by paul on 12.06.16.
 */
public class LinkedHashSetTest {
    public static void main(String[] args) {
        testAdd();
    }

    private static void testAdd() {
        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
        Assert.assertEquals("LinkedHashSetTest.addTrue", true, hashSet.add(12));
        Assert.assertEquals("LinkedHashSetTest.addFalse", false, hashSet.add(12));
    }
}
