package com.getjavajob.training.algo08.pogodaevpn.lesson10;

import static com.getjavajob.training.algo08.pogodaevpn.lesson10.BubbleSort.sortBubble;
import static com.getjavajob.training.algo08.pogodaevpn.util.Assert.assertEquals;

/**
 * Created by paul on 11.08.16.
 */
public class BubbleSortTest {
    public static void main(String[] args) {
        testBubbleSort();
    }

    private static void testBubbleSort() {
        int[] sorted = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] unSort = new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] sort = sortBubble(unSort);
        assertEquals("BubbleSortTest.testBubbleSort", sorted, sort);
    }
}
