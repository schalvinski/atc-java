package com.getjavajob.training.algo07.hafizov.lesson00;

/**
 * Created by admin on 09.08.2015.
 */
public class Task06 {
    public static void main(String[] args) {
        //System.out.println(pow2(2, 2));
        System.out.println(setNthBit1(0b110000001, 3));
    }

    /**
     * two raised to a power
     * @param m
     * @return
     */
    public static int pow(int m) {//a
        return m < 31 && m > 0 ? 2 << m - 1 : 0;
    }

    /**
     *two different numbers with different degrees of stacks
     * @param n
     * @param m
     * @return
     */
    public static int pow2(int n, int m) {//b
        return m < 31 && m > 0 ? (2 << n - 1) + (2 << m - 1) : 0;
    }

    /**
     *  reset n lower bits
     * @param a
     * @param n
     * @return
     */
    public static int mask(int a, int n){//c
        a &= ~((int)Math.pow(2.0, (double)n) - 1);
        return Integer.parseInt(Integer.toString(a,2));
    }

    /**
     *  set a's n-th bit with 1
     * @param a
     * @param n
     * @return
     */
    public static int setNthBit1(int a, int n){//d
        a |= 1<<n;
        return Integer.parseInt(Integer.toString(a,2));
    }

    /**
     * invert n-th bit
     * @param a
     * @param n
     * @return
     */
    public static int invertNthBit(int a,int n){//e
        a ^= 1<<n;
        return Integer.parseInt(Integer.toString(a,2));
    }

    /**
     * set a's n-th bit with 0
     * @param a
     * @param n
     * @return
     */
    public static int setNthBit0(int a, int n){//f
        a &= ~(1<<n);
        return Integer.parseInt(Integer.toString(a,2));
    }

    /**
     * return n lower bits
     * @param a
     * @param n
     * @return
     */
    public static int returnNLowerBits(int a, int n){//g
        String str ="";
        for (int i=n;i>=0;i--){
            str += (a>>i)&1;
        }
        return Integer.parseInt(str);
    }

    public static int returnNthBit(int a, int n){//h
        a &= (a>>n)&1;
        return Integer.parseInt(Integer.toString(a,2));
    }

}
