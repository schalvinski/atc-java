package com.getjavajob.training.algo07.hafizov.lesson00;

import com.getjavajob.training.algo07.util.Assert;

/**
 * Created by admin on 12.08.2015.
 */
public class Task06Test {
    public static void main(String[] args) {
        testPow();
        testPow2();
        testMask();
        testSetNthBit1();
        testInvertNthBit();
        testSetNthBit0();
        testReturnNLowerBits();
        testReturnNthBit();
    }

    public static void testPow(){
        Assert.assertEquals(32,Task06.pow(5));
    }

    public static void testPow2(){
        Assert.assertEquals(64,Task06.pow2(5, 5));
    }

    public static void testMask(){
        Assert.assertEquals(110011000, Task06.mask(0b110011001, 3));
    }

    public static void testSetNthBit1(){
        Assert.assertEquals(110011101,Task06.setNthBit1(0b110011101, 3));
    }

    public static void testInvertNthBit(){
        Assert.assertEquals(110010101,Task06.invertNthBit(0b110011101, 3));
    }
    public static void testSetNthBit0(){
        Assert.assertEquals(110010101,Task06.setNthBit0(0b110011101, 3));
    }

    public static void testReturnNLowerBits(){
        Assert.assertEquals(1101,Task06.returnNLowerBits(0b110011101, 3));
    }

    public static void testReturnNthBit(){
        Assert.assertEquals(1,Task06.returnNthBit(0b110011101, 3));
    }

}
