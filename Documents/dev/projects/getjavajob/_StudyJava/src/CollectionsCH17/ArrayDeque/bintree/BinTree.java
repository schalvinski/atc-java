package CollectionsCH17.ArrayDeque.bintree;

import java.util.*;

public class BinTree {

    private static <T> Node<T> makeBalancedHelperConvertDoubleLinkedListToTree(Node<T> node, boolean goLeft, int length) {
        if (length <= 1) {
            return node;
        }
        int ceilHeight;
        for (ceilHeight = 1; ((1 << (ceilHeight + 1)) - 1) < length; ++ceilHeight) {
            // nothing
        }
        int leftLength, rightLength;
        if ((1 << (ceilHeight + 1)) - 1 == length) {
            leftLength = rightLength = (1 << ceilHeight) - 1;
        } else if (1 + ((1 << ceilHeight) - 1) + ((1 << (ceilHeight - 1)) - 1) < length) {
            leftLength = (1 << ceilHeight) - 1;
            rightLength = length - 1 - leftLength;
        } else {
            rightLength = (1 << (ceilHeight - 1)) - 1;
            leftLength = length - 1 - rightLength;
        }
        if (goLeft) {
            for (int i = 0; i < rightLength; ++i) {
                node = node.left();
            }
        } else {
            for (int i = 0; i < leftLength; ++i) {
                node = node.right();
            }
        }
        if (node.left() != null) {
            node.left().setRight(null);
            node.setLeft(makeBalancedHelperConvertDoubleLinkedListToTree(node.left(), true, leftLength));
        }
        if (node.right() != null) {
            node.right().setLeft(null);
            node.setRight(makeBalancedHelperConvertDoubleLinkedListToTree(node.right(), false, rightLength));
        }
        return node;
    }
    private static <T> Node<T> makeBalancedHelperConvertTreeToDoubleLinkedList(Node<T> node, Node<T> prefixTail, int[] length) {
        if (node == null) {
            return prefixTail;
        }
        ++length[0];
        prefixTail = makeBalancedHelperConvertTreeToDoubleLinkedList(node.left(), prefixTail, length);
        if (prefixTail != null) {
            prefixTail.setRight(node);
        }
        node.setLeft(prefixTail);
        return makeBalancedHelperConvertTreeToDoubleLinkedList(node.right(), node, length);
    }
    public static <T> Node<T> makeBalanced(Node<T> node) {
        if (node == null) {
            return null;
        }
        int[] length = new int[1];
        node = makeBalancedHelperConvertTreeToDoubleLinkedList(node, null, length);
        return makeBalancedHelperConvertDoubleLinkedListToTree(node, true, length[0]);
    }

    public static <T> Iterable<Node<T>> preOrder(Node<T> node) {
        final Deque<Node<T>> stack = new ArrayDeque<>();
        if (node != null) {
            stack.push(node);
        }
        return new Iterable<Node<T>>() {
            @Override
            public Iterator<Node<T>> iterator() {
                return new Iterator<Node<T>>() {
                    @Override
                    public boolean hasNext() {
                        return !stack.isEmpty();
                    }
                    @Override
                    public Node<T> next() {
                        Node<T> next = stack.pop();
                        if (next.right() != null) {
                            stack.push(next.right());
                        }
                        if (next.left() != null) {
                            stack.push(next.left());
                        }
                        return next;
                    }
                };
            }
        };
    }

    public static <T> Iterable<Node<T>> inOrder(Node<T> node) {
        final Deque<Node<T>> stack = new ArrayDeque<>();
        for (; node != null; node = node.left()) {
            stack.push(node);
        }
        return new Iterable<Node<T>>() {
            @Override
            public Iterator<Node<T>> iterator() {
                return new Iterator<Node<T>>() {
                    @Override
                    public boolean hasNext() {
                        return !stack.isEmpty();
                    }
                    @Override
                    public Node<T> next() {
                        Node<T> next = stack.pop();
                        for (Node<T> node = next.right(); node != null; node = node.left()) {
                            stack.push(node);
                        }
                        return next;
                    }
                };
            }
        };
    }

    public static <T> Iterable<Node<T>> postOrder(Node<T> node) {
        final Deque<Node<T>> stack = new ArrayDeque<>();
        final Set<Node<T>> visitedRight = new HashSet<>();
        for (; node != null; node = node.left()) {
            stack.push(node);
        }
        return new Iterable<Node<T>>() {
            @Override
            public Iterator<Node<T>> iterator() {
                return new Iterator<Node<T>>() {
                    @Override
                    public boolean hasNext() {
                        return !stack.isEmpty();
                    }
                    @Override
                    public Node<T> next() {
                        Node<T> next = stack.pop();
                        if (visitedRight.remove(next)) {
                            return next;
                        } else {
                            while (next.right() != null) {
                                stack.push(next);
                                for (Node<T> node = next.right(); node != null; node = node.left()) {
                                    stack.push(node);
                                }
                                visitedRight.add(next);
                                next = stack.pop();
                            }
                            return next;
                        }
                    }
                };
            }
        };
    }

    private static boolean isDegenerateUnchecked(Node<?> node) {
        return (node.left() == null && node.right() == null) ||
                (node.left() == null && node.right() != null && isDegenerateUnchecked(node.right())) ||
                (node.left() != null && node.right() == null && isDegenerateUnchecked(node.left()));
    }
    public static boolean isDegenerate(Node<?> node) {
        if (node == null) {
            return true;
        }
        return isDegenerateUnchecked(node);
    }

    private static void isBalancedHelper(Node<?> node, int[] nodeCount, int curDepth, int[] maxDepth) {
        ++nodeCount[0];
        if (curDepth > maxDepth[0]) {
            maxDepth[0] = curDepth;
        }
        if (node.left() != null) {
            isBalancedHelper(node.left(), nodeCount, curDepth + 1, maxDepth);
        }
        if (node.right() != null) {
            isBalancedHelper(node.right(), nodeCount, curDepth + 1, maxDepth);
        }
    }
    public static boolean isBalanced(Node<?> node) {
        if (node == null) {
            return true;
        }
        int[] nodeCount = new int[1];
        int[] maxDepth = new int[1];
        isBalancedHelper(node, nodeCount, 0, maxDepth);
        return (1 << maxDepth[0]) - 1 < nodeCount[0];
    }

    private static boolean isPerfectHelper(Node<?> node, int curDepth, int[] maxDepth) {
        if ((node.left() == null && node.right() != null) ||
            (node.left() != null && node.right() == null)) {
            return false;
        }
        if (node.left() == null && node.right() == null) {
            if (maxDepth[0] == 0) {
                maxDepth[0] = curDepth;
                return true;
            } else {
                return curDepth == maxDepth[0];
            }
        }
        return isPerfectHelper(node.left(), curDepth + 1, maxDepth) &&
                isPerfectHelper(node.right(), curDepth + 1, maxDepth);
    }
    public static boolean isPerfect(Node<?> node) {
        if (node == null) {
            return true;
        }
        return isPerfectHelper(node, 0, new int[1]);
    }

    private static void isCompleteHelper(Node<?> node, int[] nodeCount, int curIndex, int[] maxIndex) {
        ++nodeCount[0];
        if (curIndex > maxIndex[0]) {
            maxIndex[0] = curIndex;
        }
        if (node.left() != null) {
            isCompleteHelper(node.left(), nodeCount, 2*curIndex + 1, maxIndex);
        }
        if (node.right() != null) {
            isCompleteHelper(node.right(), nodeCount, 2*curIndex + 2, maxIndex);
        }
    }
    public static boolean isComplete(Node<?> node) {
        if (node == null) {
            return true;
        }
        int[] nodeCount = new int[1];
        int[] maxIndex = new int[1];
        isCompleteHelper(node, nodeCount, 0, maxIndex);
        return maxIndex[0] < nodeCount[0];
    }

    private static boolean isFullUnchecked(Node<?> node) {
        return (node.right() == null && node.left() == null) ||
            (node.right() != null && node.left() != null && isFullUnchecked(node.right()) && isFullUnchecked(node.left()));
    }
    public static boolean isFull(Node<?> node) {
        if (node == null) {
            return true;
        }
        return isFullUnchecked(node);
    }

    // ─ │ ┌ ┐ └ ┘ ┤
    private static void toStringHelper(Node<?> node, boolean isRight, StringBuilder sb, StringBuilder prefix) {
        String suffix = " " + node.key().toString() +
                        (node.right() == null ? (node.left() == null ? "" : " ┐") : (node.left() == null ? " ┘" : " ┤"));
        int prefixLength = prefix.length();

        if (node.right() != null) {
            prefix.append(isRight ? ' ' : '│');
            for (int i = 0; i < suffix.length(); ++i) {
                prefix.append(' ');
            }
            toStringHelper(node.right(), true, sb, prefix);
        }

        prefix.delete(prefixLength, prefix.length());
        sb.append(prefix);
        sb.append(isRight ? '┌' : '└');
        sb.append('─');
        sb.append(suffix);
        sb.append('\n');

        if (node.left() != null) {
            prefix.append(isRight ? '│' : ' ');
            for (int i = 0; i < suffix.length(); ++i) {
                prefix.append(' ');
            }
            toStringHelper(node.left(), false, sb, prefix);
        }

    }
    public static String toString(Node<?> node) {
        if (node == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder prefix = new StringBuilder();
        toStringHelper(node, false, sb, prefix);
        return sb.toString();
    }

    static class NodeImpl<T> implements Node<T> {
        private T key;
        private Node<T> left;
        private Node<T> right;
        public NodeImpl(T key) {
            this.key = key;
        }
        public void attach(Node<T> left, Node<T> right) {
            this.left = left;
            this.right = right;
        }
        @Override
        public T key() {
            return key;
        }
        @Override
        public Node<T> left() {
            return left;
        }
        @Override
        public Node<T> right() {
            return right;
        }
        @Override
        public Node<T> setLeft(Node<T> node) {
            Node<T> previous = this.left;
            this.left = node;
            return previous;
        }
        @Override
        public Node<T> setRight(Node<T> node) {
            Node<T> previous = this.right;
            this.right = node;
            return previous;
        }
    }

    public static void main(String[] args) {
        @SuppressWarnings("unchecked")
        NodeImpl<Character>[] charNodes = new NodeImpl[26];
        for (char c = 'A'; c <= 'Z'; ++c) {
            charNodes[c - 'A'] = new NodeImpl<>(c);
        }
        charNodes[0].attach(charNodes[2], charNodes[17]);
        charNodes[2].attach(charNodes[1], charNodes[3]);
        charNodes[3].attach(charNodes[13], charNodes[14]);
        charNodes[17].attach(null, charNodes[24]);
        charNodes[24].attach(charNodes[21], null);
        charNodes[21].attach(charNodes[19], charNodes[18]);
        charNodes[18].attach(charNodes[25], null);
        Node<Character> root = charNodes[0];
        System.out.println(toString(root));
        System.out.println("isFull: " + isFull(root));
        System.out.println("isComplete: " + isComplete(root));
        System.out.println("isPerfect: " + isPerfect(root));
        System.out.println("isBalanced: " + isBalanced(root));
        System.out.println("isDegenerate: " + isDegenerate(root));
        for (Node<Character> node : preOrder(root)) {
            System.out.print(node.key() + ", ");
        }; System.out.println();
        for (Node<Character> node : inOrder(root)) {
            System.out.print(node.key() + ", ");
        }; System.out.println();
        for (Node<Character> node : postOrder(root)) {
            System.out.print(node.key() + ", ");
        }; System.out.println();
        root = makeBalanced(root);
        System.out.println(toString(root));
        System.out.println("isBalanced: " + isBalanced(root));
    }

}
