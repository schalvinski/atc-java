package tree;

import java.util.ArrayList;
import java.util.List;

public class Node<T> {
	private T data;
	private Node<T> parent;
	private List<Node<T>> childs = new ArrayList<Node<T>>();
	
	public Node(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean addChild(T obj) {
		return addChild(new Node<T>(obj));
	}
	
	public boolean addChild(Node<T> node) {
		node.setParent(this);
		return childs.add(node);
	}
	
	public Node<T> setChild(int index, Node<T> node){
		while(childs.size() <= index){
			childs.add(null);
		}
		
		if(node != null) node.setParent(this);
		return childs.set(index, node);
	}
	
	public Node<T> getChild(int index) {
		if(childs.size() > index)
			return childs.get(index);
		
		return null;
	}

	public List<Node<T>> getAllChilds() {
		return childs;
	}

	public Node<T> removeChild(int index) {
		return childs.remove(index);
	}

	public Node<T> getParent() {
		return parent;
	}

	public void setParent(Node<T> parent) {
		this.parent = parent;
	}
	
	public String getTreeString() {
		return getTreeString("");
	}
	
	public String getTreeString(String prefix) {
		String ret = toString()+"\n";
		for (int i=0; i< childs.size(); i++) {
			Node<T> node = childs.get(i);
			if(node == null) continue;
			
			if(i <  childs.size()-1)
				ret += prefix + "┣" + node.getTreeString(prefix+ "┃");
			else
				ret += prefix + "┗" + node.getTreeString(prefix+ " ");
		}
		
		return ret;
	}

	@Override
	public String toString() {
		return "["+ data + "]";
	}
}
