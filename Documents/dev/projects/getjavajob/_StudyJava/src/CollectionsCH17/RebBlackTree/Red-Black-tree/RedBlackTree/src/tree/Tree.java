package tree;

import java.util.Iterator;
import java.util.Stack;

public class Tree<T> implements Iterable<T> {
	private Node<T> root;
	
	public class PreOrderIterator implements Iterator<T>{
		private Stack<Integer> stack = new Stack<Integer>();
		private Node<T> next;
		private int idx;
		
		public PreOrderIterator() {
			next = root;
			stack.push(0);
		}

		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public T next() {
			Node<T> ret = next;
			
			if(stack.isEmpty()) return null;
			
			idx = stack.pop();
			if(idx >= next.getAllChilds().size()){
				next = next.getParent();
				T nextValue = next();
				if(nextValue != null){
					return nextValue;
				}else{
					return ret.getData();
				}
			}
			
			stack.push(idx+1);
			stack.push(0);
			next = next.getChild(idx);
			
			return ret.getData();
		}
	}
	
	public Tree() {}
	
	public Tree(Node<T> root) {
		this.root = root;
	}

	public Node<T> getRoot() {
		return root;
	}

	public void setRoot(Node<T> root) {
		this.root = root;
	}
	
	public String getTreeString() {
		if(root == null) return "";
		return root.getTreeString();
	}

	@Override
	public Iterator<T> iterator() {
		return new PreOrderIterator();
	}
	
	public int size() {
		int count = 0;
		for (@SuppressWarnings("unused") T node : this) {
			count++;
		}
		return count;
	}
}
