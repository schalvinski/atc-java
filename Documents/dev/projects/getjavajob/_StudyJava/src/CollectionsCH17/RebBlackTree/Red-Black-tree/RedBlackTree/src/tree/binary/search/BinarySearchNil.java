package tree.binary.search;

import tree.Nil;


public class BinarySearchNil<Key extends Comparable<Key>, Value> extends BinarySearchNode<Key, Value> implements Nil{
	public BinarySearchNil() {
		super(null);
	}
	public String getTreeString(){
		return toString();
	}
	@Override
	public String toString() {
		return "[nil]";
	}
}