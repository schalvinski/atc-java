package tree.binary.search;

import tree.Nil;
import tree.Node;

public class BinarySearchNode<Key extends Comparable<Key>, Value> extends Node<Value>
		implements Comparable<BinarySearchNode<Key, Value>> {
	
	private Key key;

	public BinarySearchNode(Key key) {
		this(key,null);
	}

	public BinarySearchNode(Key key, Value data) {
		super(data);
		this.key = key;
	}

	public Key getKey() {
		return key;
	}

	public BinarySearchNode<Key, Value> setLeftChild(Key key){
		return setLeftChild(key, null);
	}
	
	public BinarySearchNode<Key, Value> setLeftChild(Key key, Value value){
		return setLeftChild(new BinarySearchNode<Key, Value>(key, value));
	}
	
	@SuppressWarnings("unchecked")
	public BinarySearchNode<Key, Value> setLeftChild(BinarySearchNode<Key, Value> node){
		return (BinarySearchNode<Key, Value>) super.setChild(0, node);
	}
	
	public BinarySearchNode<Key, Value> setRightChild(Key key){
		return setRightChild(key, null);
	}
	
	public BinarySearchNode<Key, Value> setRightChild(Key key, Value value){
		return setRightChild(new BinarySearchNode<Key, Value>(key, value));
	}
	
	@SuppressWarnings("unchecked")
	public BinarySearchNode<Key, Value> setRightChild(BinarySearchNode<Key, Value> node){
		return (BinarySearchNode<Key, Value>) super.setChild(1, node);
	}
	
	@SuppressWarnings("unchecked")
	public BinarySearchNode<Key, Value> getLeftChild(){
		return (BinarySearchNode<Key, Value>) super.getChild(0);
	}

	@SuppressWarnings("unchecked")
	public BinarySearchNode<Key, Value> getRightChild(){
		return (BinarySearchNode<Key, Value>) super.getChild(1);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BinarySearchNode<Key, Value> getParent() {
		return (BinarySearchNode<Key, Value>) super.getParent();
	}
	
	public BinarySearchNode<Key, Value> getMinimum(){
		BinarySearchNode<Key, Value> current = this;
		while(!(current.getLeftChild() instanceof Nil)){
			current = current.getLeftChild();
		}
		return current;
	}
	
	public BinarySearchNode<Key, Value> getMaximum(){
		BinarySearchNode<Key, Value> current = this;
		while(!(current.getRightChild() instanceof Nil)){
			current = current.getRightChild();
		}
		return current;
	}

	public boolean addChild(BinarySearchNode<Key, Value> node) {
		if(super.getAllChilds().size() > 1)
			throw new IllegalStateException("Number of child nodes is maximum.");
		return super.addChild(node);
	}
	

	@Override
	public int compareTo(BinarySearchNode<Key, Value> o) {
		return key.compareTo(o.getKey());
	}
	
	@Override
	public String toString() {
		return "["+ key.toString() +":"+ getData().toString() + "]";
	}
	
	public String getTreeString(String prefix) {
		String ret = toString()+"\n";
		for (int i=getAllChilds().size()-1; i>= 0; i--) {
			Node<Value> node = getChild(i);
			
			if(i >  0)
				ret += prefix + "┣R" + node.getTreeString(prefix+ "┃");
			else
				ret += prefix + "┗L" + node.getTreeString(prefix+ " ");
		}
		
		return ret;
	}
}