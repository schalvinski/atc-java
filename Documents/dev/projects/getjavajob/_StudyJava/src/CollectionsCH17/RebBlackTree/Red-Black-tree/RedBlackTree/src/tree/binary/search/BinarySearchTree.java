package tree.binary.search;

import java.util.Iterator;
import java.util.NoSuchElementException;

import tree.Nil;
import tree.Tree;

public class BinarySearchTree<Key extends Comparable<Key>, Value> extends Tree<Value> {
	private BinarySearchNode<Key, Value> nil = new BinarySearchNil<Key, Value>();
	
	public BinarySearchTree(){}
	
	public BinarySearchTree(Key key, Value data){
		this(new BinarySearchNode<Key, Value>(key, data));
	}
	
	public BinarySearchTree(BinarySearchNode<Key, Value> binarySearchNode) {
		super(binarySearchNode);
	}
	
	public void setRoot(BinarySearchNode<Key, Value> root) {
		super.setRoot(root);
		initNode(root);
		root.setParent(nil);
	}

	@Override
	@SuppressWarnings("unchecked")
	public BinarySearchNode<Key, Value> getRoot() {
		return (BinarySearchNode<Key, Value>) super.getRoot();
	}
	
	protected void initNode(BinarySearchNode<Key, Value> node){
		if(node.getLeftChild() == null)
			node.setLeftChild(nil);
		if(node.getRightChild() == null)
			node.setRightChild(nil);
	}
	
	public void add(Key key, Value data) {
		BinarySearchNode<Key, Value> node = new BinarySearchNode<Key, Value>(key, data);
		initNode(node);
		add(node);
	}
	
	public void add(BinarySearchNode<Key, Value> node) {
		if(getRoot() == null){
			setRoot(node);
			return;
		}
		
		BinarySearchNode<Key, Value> current = (BinarySearchNode<Key, Value>) getRoot();
		
		while(true){
			if(node.compareTo(current) <= 0){
				if(current.getLeftChild() instanceof Nil ){
					current.setLeftChild(node);
					break;
				}
				current = (BinarySearchNode<Key, Value>) current.getLeftChild();
			}else{
				if(current.getRightChild() instanceof Nil){
					current.setRightChild(node);
					break;
				}
				current = (BinarySearchNode<Key, Value>) current.getRightChild();
			}
		}
	}
	
	public void delete(Key key) {
		BinarySearchNode<Key, Value> node = getNode(key);
		if(node == null)
			throw new NoSuchElementException();
		delete(node);
	}

	public void delete(BinarySearchNode<Key, Value> node) {
		if(node.getLeftChild() instanceof Nil){
			transplant(node,node.getRightChild());
		}else if(node.getRightChild() instanceof Nil){
			transplant(node,node.getLeftChild());
		}else{
			BinarySearchNode<Key, Value> successor = node.getRightChild().getMinimum();
			if(successor.getParent() == node){
				
			}else{
				transplant(successor, successor.getRightChild());
				successor.setRightChild(node.getRightChild());
			}
			successor.setLeftChild(node.getLeftChild());
			transplant(node, successor);
		}
	}
	
	public void transplant(BinarySearchNode<Key, Value> oldNode, BinarySearchNode<Key, Value> plantNode){
		if(oldNode.getParent() instanceof Nil) 
			setRoot(plantNode);
		else if(oldNode == oldNode.getParent().getLeftChild())
			oldNode.getParent().setLeftChild(plantNode);
		else if(oldNode == oldNode.getParent().getRightChild())
			oldNode.getParent().setRightChild(plantNode);
	}
	
	public Value get(Key key){
		BinarySearchNode<Key, Value> node = getNode(key);
		if(node==null) throw new NoSuchElementException();
		return node.getData();
	}
	
	public BinarySearchNode<Key, Value> getNode(Key key){
		BinarySearchNode<Key, Value> currentNode = getRoot();
		
		while(!(currentNode instanceof Nil)){
			int compare = key.compareTo(currentNode.getKey());
			if(compare < 0){
				currentNode = currentNode.getLeftChild();
			}else if(compare > 0){
				currentNode = currentNode.getRightChild();
			}else{
				return currentNode;
			}
		}
		
		return null;
	}
	
	public class InOrderIterator implements Iterator<Value> {
		BinarySearchNode<Key, Value> next;
		public InOrderIterator() {
			next = getMinimum();
		}

		@Override
		public boolean hasNext() {
			return !(next instanceof Nil || next == null);
		}

		@Override
		public Value next() {
			BinarySearchNode<Key, Value> ret = next;
			
			if(!(next.getRightChild() instanceof Nil)){
				next = next.getRightChild();
				while(!(next.getLeftChild() instanceof Nil)){
					next = next.getLeftChild();
				}
			}else{
				while(!(next.getParent() instanceof Nil) && next.getParent().getLeftChild() != next){
					next = next.getParent();
				}
				next = next.getParent();
			}
			
			return ret.getData();
		}
	}
	
	@Override
	public Iterator<Value> iterator() {
		return new InOrderIterator();
	}

	public BinarySearchNode<Key, Value> getMinimum() {
		if(getRoot() == null)
			return null;
		return getRoot().getMinimum();
	}
	
	public BinarySearchNode<Key, Value> getMaximum() {
		if(getRoot() == null)
			return null;
		return getRoot().getMaximum();
	}
}