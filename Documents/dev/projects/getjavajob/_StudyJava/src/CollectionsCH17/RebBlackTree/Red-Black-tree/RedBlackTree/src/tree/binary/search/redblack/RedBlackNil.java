package tree.binary.search.redblack;

import tree.Nil;


public class RedBlackNil<Key extends Comparable<Key>, Value> extends RedBlackNode<Key, Value> implements Nil{
	public RedBlackNil() {
		super(null);
		setColor(Color.BLACK);
	}
	
	public String getTreeString(){
		return toString();
	}
	
	@Override
	public String toString() {
		return "● nil";
	}
}