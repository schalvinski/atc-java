package CollectionsCH17.RebBlackTree.Red;

import tree.Nil;
import tree.binary.search.BinarySearchNode;



public class RedBlackNode<Key extends Comparable<Key>, Value> extends
		BinarySearchNode<Key, Value> {
	enum Color {
		RED, BLACK
	};

	private Color color;

	public RedBlackNode(Key key) {
		this(key, null, null);
	}

	public RedBlackNode(Key key, Color color) {
		this(key, null, color);
	}

	public RedBlackNode(Key key, Value value) {
		this(key, value, null);
	}

	public RedBlackNode(Key key, Value value, Color color) {
		super(key, value);
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	@Override
	public RedBlackNode<Key, Value> getLeftChild() {
		return (RedBlackNode<Key, Value>) super.getLeftChild();
	}

	@Override
	public RedBlackNode<Key, Value> getRightChild() {
		return (RedBlackNode<Key, Value>) super.getRightChild();
	}

	@Override
	public RedBlackNode<Key, Value> getParent() {
		BinarySearchNode<Key, Value> ret = super.getParent();
		if(ret instanceof Nil){
			ret = new RedBlackNil<Key, Value>();
		}
		return (RedBlackNode<Key, Value>) ret;
	}

	public int getBlackHeight() {
		int leftHeight = 1, 
			rightHeight = 1, 
			thisHeight = (getColor() == Color.BLACK) ? 1 : 0;

		RedBlackNode<Key, Value> left = getLeftChild();
		RedBlackNode<Key, Value> right = getRightChild();

		if (left  instanceof Nil && right  instanceof Nil) return thisHeight + 1;
		if (!(left  instanceof Nil)) leftHeight = left.getBlackHeight();
		if (!(right  instanceof Nil)) rightHeight = right.getBlackHeight();

		return (leftHeight == rightHeight) ? leftHeight + thisHeight : -1;
	}

	@Override
	public String toString() {
		String color = ""; 
		
		switch(getColor()){
		case BLACK:
			color = "●";
			break;
		case RED:
			color = "○";
			break;
		}
		
		return color + super.toString();
	}

	public boolean isRightColored() {
		if(!(getLeftChild() instanceof Nil) && !getLeftChild().isRightColored())
			return false;
		if(!(getRightChild() instanceof Nil) && !getRightChild().isRightColored())
			return false;
		if(getColor() == Color.RED){
			if(getParent() instanceof Nil) 
				return false;
			if(!(getLeftChild() instanceof Nil) && getLeftChild().getColor() == Color.RED)
				return false;
			if(!(getRightChild() instanceof Nil) && getRightChild().getColor() == Color.RED)
				return false;
		}else if(getColor() != Color.BLACK){
			return false;
		}
		return true;
	}
}