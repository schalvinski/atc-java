package tree.binary.search.redblack;

import java.util.NoSuchElementException;

import tree.Nil;
import tree.binary.search.BinarySearchNode;
import tree.binary.search.BinarySearchTree;
import tree.binary.search.redblack.RedBlackNode.Color;

public class RedBlackTree<Key extends Comparable<Key>, Value> extends BinarySearchTree<Key, Value> {

	protected RedBlackNode<Key, Value> nil = new RedBlackNil<Key, Value>();
	
	public RedBlackTree(){}
	
	public RedBlackTree(Key key, Value data){
		super(key, data);
	}

	public RedBlackTree(RedBlackNode<Key, Value> redBlackNode) {
		super(redBlackNode);
	}
	
	@Override
	public RedBlackNode<Key,Value> getRoot(){
		return (RedBlackNode<Key,Value>) super.getRoot();
	}
	
	public void setRoot(RedBlackNode<Key, Value> root) {
		super.setRoot(root);
		root.setParent(nil);
		root.setColor(Color.BLACK);
	}
	
	@Override
	protected void initNode(BinarySearchNode<Key, Value> node){
		if(node.getParent() == null)
			node.setParent(nil);
		if(node.getLeftChild() == null)
			node.setLeftChild(nil);
		if(node.getRightChild() == null)
			node.setRightChild(nil);
	}
	
	@Override
	public void add(Key key, Value data) {
		RedBlackNode<Key, Value> node = new RedBlackNode<Key, Value>(key, data);
		initNode(node);
		add(node);
	}
	
	public void add(RedBlackNode<Key, Value> node) {
		if(getRoot() == null){
			setRoot(node);
		}else{
			node.setColor(Color.RED);
			super.add(node);
			fixup(node);
		}
	}
	
	@Override
	public RedBlackNode<Key, Value> getNode(Key key){
		return (RedBlackNode<Key, Value>)super.getNode(key);
	}
	
	@Override
	public RedBlackNode<Key, Value> getMinimum() {
		return (RedBlackNode<Key, Value>) super.getMinimum();
	}
	
	@Override
	public RedBlackNode<Key, Value> getMaximum() {
		return (RedBlackNode<Key, Value>) super.getMaximum();
	}
	
	@Override
	public void delete(Key key) {
		RedBlackNode<Key, Value> node = getNode(key);
		if(node == null)
			throw new NoSuchElementException();
		delete(node);
	}

	
	public void delete(RedBlackNode<Key, Value> node) {
		RedBlackNode<Key, Value> fixupNode = null;
		Color erasedColor = node.getColor();
		if(node.getLeftChild() instanceof Nil){
			transplant(node,node.getRightChild());
			fixupNode = node.getRightChild();
		}else if(node.getRightChild() instanceof Nil){
			transplant(node,node.getLeftChild());
			fixupNode = node.getLeftChild();
		}else{
			RedBlackNode<Key, Value> successor = (RedBlackNode<Key, Value>) node.getRightChild().getMinimum();
			erasedColor = successor.getColor();
			fixupNode = successor.getRightChild();
			if(successor.getParent() == node){
				fixupNode.setParent(successor);
			}else{
				transplant(successor, successor.getRightChild());
				successor.setRightChild(node.getRightChild());
			}
			successor.setLeftChild(node.getLeftChild());
			transplant(node, successor);
			successor.setColor(node.getColor());
		}
		if(erasedColor == Color.BLACK)
			deleteFixup(fixupNode);
	}
	
	private void deleteFixup(RedBlackNode<Key, Value> node) {
		RedBlackNode<Key, Value> sibling;
		while(node != getRoot() && node.getColor() == Color.BLACK ){
			if(node == node.getParent().getLeftChild()){
				sibling = node.getParent().getRightChild();
				if(sibling.getColor() == Color.RED){
					sibling.setColor(Color.BLACK);
					node.getParent().setColor(Color.RED);
					leftRotate(node.getParent());
					sibling = node.getParent().getRightChild();
				}
				if(sibling.getLeftChild().getColor() == Color.BLACK &&
					sibling.getRightChild().getColor() == Color.BLACK ){
					sibling.setColor(Color.RED);
					node = node.getParent();
				}else{
					if(sibling.getRightChild().getColor() == Color.BLACK){
						RedBlackNode<Key, Value> parent = node.getParent();
						sibling.getLeftChild().setColor(Color.BLACK);
						sibling.setColor(Color.RED);
						rightRotate(sibling);
						node.setParent(parent); //rotate 시 fixup node 인 node 가 nil 일 때 parent 가 변경되는 문제 해결.
						sibling = node.getParent().getRightChild();
					}
					sibling.setColor(node.getParent().getColor());
					node.getParent().setColor(Color.BLACK);
					sibling.getRightChild().setColor(Color.BLACK);
					leftRotate(node.getParent());
					node = getRoot();
				}
			}else{
				sibling = node.getParent().getLeftChild();
				if(sibling.getColor() == Color.RED){
					sibling.setColor(Color.BLACK);
					node.getParent().setColor(Color.RED);
					rightRotate(node.getParent());
					sibling = node.getParent().getLeftChild();
				}
				if(sibling.getRightChild().getColor() == Color.BLACK &&
					sibling.getLeftChild().getColor() == Color.BLACK ){
					sibling.setColor(Color.RED);
					node = node.getParent();
				}else{
					if(sibling.getLeftChild().getColor() == Color.BLACK){
						RedBlackNode<Key, Value> parent = node.getParent();
						sibling.getRightChild().setColor(Color.BLACK);
						sibling.setColor(Color.RED);
						leftRotate(sibling);
						node.setParent(parent); //rotate 시 fixup node 인 node 가 nil 일 때 parent 가 변경되는 문제 해결.
						sibling = node.getParent().getLeftChild();
					}
					sibling.setColor(node.getParent().getColor());
					node.getParent().setColor(Color.BLACK);
					sibling.getLeftChild().setColor(Color.BLACK);
					rightRotate(node.getParent());
					node = getRoot();
				}
			}
		}
		node.setColor(Color.BLACK);
	}

	public boolean isBalanced() {
		return getRoot().getBlackHeight() > 0;
	}
	
	public boolean isRightColored() {
		return getRoot().isRightColored();
	}
	
	private void fixup(RedBlackNode<Key, Value> node) {
		while(node.getParent().getColor() == Color.RED){
			if(node.getParent() == node.getParent().getParent().getLeftChild()){
				RedBlackNode<Key, Value> uncle = node.getParent().getParent().getRightChild();
				if(uncle.getColor() == Color.RED){
					node.getParent().setColor(Color.BLACK);
					uncle.setColor(Color.BLACK);
					node.getParent().getParent().setColor(Color.RED);
					node = node.getParent().getParent();
				}else{
					if(node.getParent().getRightChild() == node){
						node = node.getParent();
						leftRotate(node);
					}
					node.getParent().setColor(Color.BLACK);
					node.getParent().getParent().setColor(Color.RED);
					rightRotate(node.getParent().getParent());
				}
			}else{
				RedBlackNode<Key, Value> uncle = node.getParent().getParent().getLeftChild();
				if(uncle.getColor() == Color.RED){
					node.getParent().setColor(Color.BLACK);
					uncle.setColor(Color.BLACK);
					node.getParent().getParent().setColor(Color.RED);
					node = node.getParent().getParent();
				}else{
					if(node.getParent().getLeftChild() == node){
						node = node.getParent();
						rightRotate(node);
					}
					node.getParent().setColor(Color.BLACK);
					node.getParent().getParent().setColor(Color.RED);
					leftRotate(node.getParent().getParent());
				}
			}
		}
		getRoot().setColor(Color.BLACK);
	}
	
	private void leftRotate(RedBlackNode<Key, Value> pivot){
		RedBlackNode<Key, Value> target = pivot.getRightChild();
		RedBlackNode<Key, Value> child = target.getLeftChild();
		RedBlackNode<Key, Value> parent = pivot.getParent();
		
		pivot.setRightChild(child);
		target.setLeftChild(pivot);	

		if(parent instanceof Nil){
			setRoot(target);
			target.setParent(parent);
		}else if(parent.getLeftChild() == pivot){
			parent.setLeftChild(target);
		}else if(parent.getRightChild() == pivot){
			parent.setRightChild(target);
		}
	}
	
	private void rightRotate(RedBlackNode<Key, Value> pivot){
		RedBlackNode<Key, Value> target = pivot.getLeftChild();
		RedBlackNode<Key, Value> child = target.getRightChild();
		RedBlackNode<Key, Value> parent = pivot.getParent();
		
		pivot.setLeftChild(child);
		target.setRightChild(pivot);	

		if(parent instanceof Nil){
			setRoot(target);
			target.setParent(parent);
		}else if(parent.getLeftChild() == pivot){
			parent.setLeftChild(target);
		}else if(parent.getRightChild() == pivot){
			parent.setRightChild(target);
		}
	}
}