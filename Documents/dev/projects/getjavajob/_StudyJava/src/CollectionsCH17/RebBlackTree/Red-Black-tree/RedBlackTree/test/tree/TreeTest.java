package tree;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;


public class TreeTest {

	@Test
	public void create() {
		Tree<Integer> t = new Tree<Integer>(new Node<Integer>(1));
		assertTrue(t instanceof Tree);
	}
	
	@Test
	public void root() {
		Tree<Integer> t = new Tree<Integer>(new Node<Integer>(1));
		assertTrue(t.getRoot() instanceof Node<?>);
		assertEquals((int) t.getRoot().getData(),1); 
		
		t.getRoot().setData(1);
		int rootData = t.getRoot().getData();
		assertEquals(rootData , 1);
		
		t.setRoot(new Node<Integer>(2));
		rootData = t.getRoot().getData();
		assertEquals(rootData , 2);
	}
	
	@Test
	public void addChild() {
		
		Tree<Integer> t = new Tree<Integer>(new Node<Integer>(0));
		
		Node<Integer> root = t.getRoot();
		root.addChild(new Node<Integer>(3));
		root.addChild(new Node<Integer>(2));
		List<?> childs = root.getAllChilds();
		
		assertEquals(childs.size(), 2);
		assertEquals(childs.get(0), root.getChild(0));
		assertEquals(childs.get(1), root.getChild(1));
		
		Node<?> removedNode = root.removeChild(0);
		assertEquals(childs.size(), 1);
		assertEquals(3,removedNode.getData());
		if(root.getChild(0).getData() == 2){
			assertTrue(true);	
		}
		
		
		assertEquals(root.getChild(0).getParent(),root);
	}
	
	@Test
	public void drawTree() {
		Tree<Integer> t = new Tree<Integer>(new Node<Integer>(0));
		Node root = t.getRoot();
		root.addChild(1);
		root.getChild(0).addChild(1);
		root.getChild(0).addChild(1);
		root.getChild(0).getChild(0).addChild(1);
		root.getChild(0).getChild(0).addChild(1);
		root.getChild(0).addChild(2);
		root.getChild(0).addChild(2);
		root.getChild(0).getChild(3).addChild(3);
		root.getChild(0).getChild(3).addChild(4);
		root.getChild(0).getChild(3).getChild(1).addChild(3);
		root.getChild(0).getChild(3).getChild(1).addChild(3);
		root.addChild(1);
		root.addChild(1);
		root.getChild(1).addChild(1);
		root.getChild(1).addChild(1);
		root.getChild(1).addChild(1);
		root.getChild(1).addChild(1);
		root.addChild(1);
		root.addChild(1);
		root.getChild(2).addChild(1);
		root.getChild(2).addChild(1);
		root.getChild(2).addChild(1);
		root.getChild(2).addChild(1);
		System.out.println(t.getTreeString()); 
	}
	
	@Test
	public void iterrate() {
		Tree<Integer> t = new Tree<Integer>(new Node<Integer>(0));
		Node root = t.getRoot();
		
		
		
		root.addChild(1);
		root.getChild(0).addChild(11);
		root.getChild(0).getChild(0).addChild(111);
		root.getChild(0).getChild(0).addChild(112);
		root.getChild(0).addChild(12);
		root.getChild(0).addChild(13);
		root.getChild(0).addChild(14);
		root.getChild(0).getChild(3).addChild(141);
		root.getChild(0).getChild(3).addChild(142);
		root.getChild(0).getChild(3).getChild(1).addChild(1411);
		root.getChild(0).getChild(3).getChild(1).addChild(1412);
		root.addChild(2);
		root.addChild(3);
		root.getChild(2).addChild(31);
		root.getChild(2).addChild(32);
		root.getChild(2).addChild(33);
		root.getChild(2).addChild(34);
		root.addChild(4);
		root.addChild(5);
		root.getChild(4).addChild(51);
		root.getChild(4).addChild(52);
		root.getChild(4).addChild(53);
		root.getChild(4).addChild(54);

		int i=0;
		for (Iterator<Integer> iterator = t.iterator(); iterator.hasNext();) {
			int n = iterator.next();
			System.out.print((i++)+" : ");
			System.out.println(n);
			if( i>100) break;
		}
		
		
		
	}

}
