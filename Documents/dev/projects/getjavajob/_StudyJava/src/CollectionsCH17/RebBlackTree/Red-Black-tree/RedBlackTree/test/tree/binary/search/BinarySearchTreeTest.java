package tree.binary.search;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

public class BinarySearchTreeTest {


	private boolean isSorted(BinarySearchTree<Integer, Integer> rbt) {
		int prev = Integer.MIN_VALUE;
		int curr = Integer.MIN_VALUE;
		for (int value : rbt) {
			prev = curr;
			curr = value;
			if(prev > curr) return false;
		}
		return true;
	}
	
	@Test
	public void adding() {
		
		BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
		bst.add(3, 3);
		System.out.println(bst.getTreeString());
		bst.add(6, 6);
		System.out.println(bst.getTreeString());
		bst.add(4, 4);
		System.out.println(bst.getTreeString());
		
	}
	
	@Test
	public void addingAndInOrderTraversing() {
		
		
		for (int i = 0; i < 1000; i++) {
			int numt = 200;
			BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				bst.add(num, num);	
			}
			
			int current = Integer.MIN_VALUE;
			int count = 0;
			for (Iterator<Integer> iterator = bst.iterator(); iterator.hasNext();) { 
				int n = iterator.next();
				assertTrue(current <= n);
				current = n;
				count++;
			}
			
			assertTrue(numt == count);
		}
		
	}
	
	@Test
	public void addingSame() {
		
		
		for (int i = -1; i < 1; i++) {
			int numt = 200;
			int num = (int)Math.floor(Math.random()*100);
			BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				bst.add(num, num);	
			}
			
			int current = Integer.MIN_VALUE;
			int count = 0;
			for (Iterator<Integer> iterator = bst.iterator(); iterator.hasNext();) { 
				int n = iterator.next();
				assertTrue(current <= n);
				current = n;
				count++;
			}
			
			assertTrue(numt == count);
		}
		
	}
	
	@Test
	public void addingReverse() {
		
		int numt = 200;
		BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
		for (int j = numt-1; j >= 0; j--) {
			bst.add(j, j);	
		}
		
		int current = Integer.MIN_VALUE;
		int count = 0;
		for (Iterator<Integer> iterator = bst.iterator(); iterator.hasNext();) { 
			int n = iterator.next();
			assertTrue(current <= n);
			current = n;
			count++;
		}
		
		assertTrue(numt == count);
	}
	
	@Test
	public void addingInOrder() {
		
		int numt = 200;
		BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
		for (int j = 0; j < numt; j++) {
			bst.add(j, j);	
		}
		
		int current = Integer.MIN_VALUE;
		int count = 0;
		for (Iterator<Integer> iterator = bst.iterator(); iterator.hasNext();) { 
			int n = iterator.next();
			assertTrue(current <= n);
			current = n;
			count++;
		}
		
		assertTrue(numt == count);
	}
	
	@Test
	public void addingNegative() {
		
		int numt = 200;
		BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
		for (int j = 0; j > -numt; j--) {
			bst.add(j, j);	
		}
		
		int current = Integer.MIN_VALUE;
		int count = 0;
		for (Iterator<Integer> iterator = bst.iterator(); iterator.hasNext();) {
			int n = iterator.next();
			assertTrue(current <= n);
			current = n;
			count++;
		}
		
		assertTrue(numt == count);
		}
	
	@Test
	public void addingRandPosNeg() {
		for (int i = 0; i < 1000; i++) {
			int numt = 200;
			BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100)-50;
				bst.add(num, num);	
			}
			
			int current = Integer.MIN_VALUE;
			int count = 0;
			for (Iterator<Integer> iterator = bst.iterator(); iterator.hasNext();) { 
				int n = iterator.next();
				assertTrue(current <= n);
				current = n;
				count++;
			}
			
			assertTrue(numt == count);
		}
	}
	
	@Test
	public void addingAndGetting() {
		
		int numt = 200;
		int keys[] = new int[200];
		
		BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
		for (int j = 0; j < numt; j++) {
			int num = (int)Math.floor(Math.random()*100);
			bst.add(num, num+1);
			keys[j] = num;
		}
			
		for (int j = 0; j < numt; j++) {
			System.out.println(keys[j]+","+bst.get(keys[j]));
			assertTrue(keys[j]+1 == bst.get(keys[j]));
		}
	}
	
	@Test
	public void deletingRandom() {
		
		for (int i = 0; i < 100; i++) {
			int numt = 200;
			List<Integer> keys = new ArrayList<Integer>();
			
			BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				bst.add(num, num);
				keys.add(num);
			}
			
			Collections.shuffle(keys);
			
			for (int j = 0; j < numt; j++) {
				bst.delete(keys.get(j));
				assertTrue(isSorted(bst));
			}
			
			assertTrue(bst.size() == 0);
		}
	}
	
	@Test
	public void deletingAsc() {
		
		for (int i = 0; i < 100; i++) {
			int numt = 10;
			List<Integer> keys = new ArrayList<Integer>();
			
			BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				bst.add(num, num);
				keys.add(num);
			}
			
			while (bst.size() > 0) {
				bst.delete(bst.getMinimum());
				assertTrue(isSorted(bst));
			}
		}
	}
	
	@Test
	public void deletingDsc() {
		
		for (int i = 0; i < 100; i++) {
			int numt = 10;
			List<Integer> keys = new ArrayList<Integer>();
			
			BinarySearchTree<Integer, Integer> bst = new BinarySearchTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				bst.add(num, num);
				keys.add(num);
			}
			
			while (bst.size() > 0) {
				bst.delete(bst.getMaximum());
				assertTrue(isSorted(bst));
			}
		}
	}
}
