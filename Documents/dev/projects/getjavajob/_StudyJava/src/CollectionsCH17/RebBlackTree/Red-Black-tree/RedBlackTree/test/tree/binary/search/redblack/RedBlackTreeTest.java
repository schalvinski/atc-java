package tree.binary.search.redblack;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import tree.Node;
import tree.binary.search.BinarySearchTree;

public class RedBlackTreeTest {

	private boolean isSorted(RedBlackTree<Integer, Integer> rbt) {
		int prev = Integer.MIN_VALUE;
		int curr = Integer.MIN_VALUE;
		for (int value : rbt) {
			prev = curr;
			curr = value;
			if(prev > curr) return false;
		}
		return true;
	}
	
	@Test
	public void addingRandom() {
		for (int i = 0; i < 1000; i++) {
			
			int numt = 200;
			int entry[] = new int[numt];
			
			RedBlackTree<Integer,Integer> rbt = new RedBlackTree<Integer,Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				entry[j] = num;
				rbt.add(num, num);
			}
			
			assertTrue(rbt.isRightColored());
			assertTrue(rbt.isBalanced());
			assertTrue(isSorted(rbt));
						
			for (int j = 0; j < numt; j++) {
				assertTrue(rbt.get(entry[j]) == entry[j]);
			}
		}
	}
	
	@Test
	public void addingAsc() {
		int numt = 200;
		
		RedBlackTree<Integer,Integer> rbt = new RedBlackTree<Integer,Integer>();
		for (int j = 0; j < numt; j++) {
			rbt.add(j, j);
		}
		
		assertTrue(rbt.isRightColored());
		assertTrue(rbt.isBalanced());
		assertTrue(isSorted(rbt));
		
		for (int j = 0; j < numt; j++) {
			assertTrue(rbt.get(j) == j);
		}
	}
	
	@Test
	public void addingDsc() {
		int numt = 200;
		RedBlackTree<Integer,Integer> rbt = new RedBlackTree<Integer,Integer>();
		for (int j = numt; j > 0 ; j--) {
			rbt.add(j,j);
		}
		
		assertTrue(rbt.isRightColored());
		assertTrue(rbt.isBalanced());
		assertTrue(isSorted(rbt));
		
		for (int j = numt; j > 0 ; j--) {
			assertTrue(rbt.get(j) == j);
		}
	}
	
	@Test
	public void addingNegativeAsc() {
		int numt = 200;
		
		RedBlackTree<Integer,Integer> rbt = new RedBlackTree<Integer,Integer>();
		for (int j = 0; j < numt; j++) {
			rbt.add(-j, -j);
		}
		
		assertTrue(rbt.isRightColored());
		assertTrue(rbt.isBalanced());
		assertTrue(isSorted(rbt));
		
		for (int j = 0; j < numt; j++) {
			assertTrue(rbt.get(-j) == -j);
		}
	}
	
	@Test
	public void addingNegativeDsc() {
		int numt = 200;
		RedBlackTree<Integer,Integer> rbt = new RedBlackTree<Integer,Integer>();
		for (int j = numt; j > 0 ; j--) {
			rbt.add(-j,-j);
		}
		
		assertTrue(rbt.isRightColored());
		assertTrue(rbt.isBalanced());
		assertTrue(isSorted(rbt));
		
		for (int j = numt; j > 0 ; j--) {
			assertTrue(rbt.get(-j) == -j);
		}
	}
	
	@Test
	public void deletingRandom() {
		
		for (int i = 0; i < 100; i++) {
			int numt = 200;
			List<Integer> keys = new ArrayList<Integer>();
			
			RedBlackTree<Integer, Integer> rbt = new RedBlackTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				rbt.add(num, num);
				keys.add(num);
			}

			Collections.shuffle(keys);
			
			for (int j = 0; j < numt; j++) {
				rbt.delete(keys.get(j));
				assertTrue(rbt.isRightColored());
				assertTrue(rbt.isBalanced());
				assertTrue(isSorted(rbt));
			}
			
			assertTrue(rbt.size() == 0);
		}
	}

	@Test
	public void deletingAsc() {
		
		for (int i = 0; i < 100; i++) {
			int numt = 10;
			List<Integer> keys = new ArrayList<Integer>();
			
			RedBlackTree<Integer, Integer> rbt = new RedBlackTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				rbt.add(num, num);
				keys.add(num);
			}
			
			while (rbt.size() > 0) {
				rbt.delete(rbt.getMinimum());
				assertTrue(rbt.isRightColored());
				assertTrue(rbt.isBalanced());
				assertTrue(isSorted(rbt));
				
			}
		}
	}
	
	
	@Test
	public void deletingDsc() {
		
		for (int i = 0; i < 100; i++) {
			int numt = 10;
			List<Integer> keys = new ArrayList<Integer>();
			
			RedBlackTree<Integer, Integer> rbt = new RedBlackTree<Integer, Integer>();
			for (int j = 0; j < numt; j++) {
				int num = (int)Math.floor(Math.random()*100);
				rbt.add(num, num);
				keys.add(num);
			}
			
			while (rbt.size() > 0) {
				rbt.delete(rbt.getMaximum());
				assertTrue(rbt.isRightColored());
				assertTrue(rbt.isBalanced());
				assertTrue(isSorted(rbt));
			}
		}
	}
} 