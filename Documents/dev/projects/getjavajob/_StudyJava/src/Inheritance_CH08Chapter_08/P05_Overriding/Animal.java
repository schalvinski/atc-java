package Inheritance_CH08Chapter_08.P05_Overriding;

/**
 * Created by user on 20.11.16.
 */
public class Animal {

    public Animal eat() {
        System.out.println("animal eat");
        return null;
    }

    public Long calc() {
        return null;
    }

}
public class Dog extends Animal {

    public Dog eat() {
        return new Dog();
    }
/*attempting to use incompatible return type
    public Integer calc() {
        return null;
    }
*/
}
