package static_P07_UnderstandingStatic.блоки_инициализации;

/**
 * Created by user on 20.11.16.
 */
public class Test {

    static {
        System.out.println("Static");
    }

    {
        System.out.println("Non-static block");
    }

    public Test() {
        System.out.println("Constructor");
    }
}
