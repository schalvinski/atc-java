package com.getjavajob.training.algo08.pogodaevpn.lesson06;

import java.util.HashMap;

/**
 * Created by paul on 26.05.16.
 */
public class MatrixImpl<V> implements Matrix<Integer> {
    public HashMap<KeyMatrix, Integer> matrix;

    public MatrixImpl(HashMap<KeyMatrix, Integer> matrix)   {
        this.matrix = matrix;
    }

    @Override
    public Integer get(int i, int j) {
        return matrix.get(new KeyMatrix(i, j));
    }

    @Override
    public void set(int i, int j, Integer value) {
        matrix.put(new KeyMatrix(i, j), value);
    }
}
