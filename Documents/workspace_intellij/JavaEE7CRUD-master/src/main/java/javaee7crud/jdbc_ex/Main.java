package javaee7crud.jdbc_ex;

/**
 * Created by user on 21.02.17.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
//from w w  w.jav a2s.  c  o  m
public class Main {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/";

    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) throws Exception {
        Connection conn = null;
        Statement stmt = null;

        Class.forName(JDBC_DRIVER);
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        stmt = conn.createStatement();

        String sql = "CREATE DATABASE STUDENTS";
        stmt.executeUpdate(sql);
        System.out.println("Database created successfully...");

        stmt.close();
        conn.close();
    }
}
