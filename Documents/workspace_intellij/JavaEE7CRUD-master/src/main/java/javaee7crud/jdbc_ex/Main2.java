package javaee7crud.jdbc_ex;

/**
 * Created by user on 21.02.17.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
/*from  w  w  w.  j  ava  2 s .  c  om*/
public class Main2 {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/";

    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) throws Exception {
        Connection conn = null;
        Statement stmt = null;

        Class.forName(JDBC_DRIVER);
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        System.out.println("Deleting database...");
        stmt = conn.createStatement();

        String sql = "DROP DATABASE STUDENTS";
        stmt.executeUpdate(sql);

        stmt.close();
        conn.close();
    }
}
