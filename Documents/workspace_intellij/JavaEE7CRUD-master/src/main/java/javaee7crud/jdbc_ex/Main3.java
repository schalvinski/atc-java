package javaee7crud.jdbc_ex;

/**
 * Created by user on 21.02.17.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
//www .j a  v  a2s . co  m
public class Main3 {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";

    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) throws Exception {
        Connection conn = null;
        Statement stmt = null;

        Class.forName(JDBC_DRIVER);
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        System.out.println("Deleting database...");
        stmt = conn.createStatement();

        conn = DriverManager.getConnection(DB_URL, USER, PASS);

        stmt = conn.createStatement();

        String sql = "CREATE TABLE Person"
                + "(id INTEGER not NULL, "
                + " firstName VARCHAR(50), "
                + " lastName VARCHAR(50), "
                + " age INTEGER, "
                + " PRIMARY KEY ( id ))";

        stmt.executeUpdate(sql);
        System.out.println("Created table in given database...");
        stmt.close();
        conn.close();
    }
}
