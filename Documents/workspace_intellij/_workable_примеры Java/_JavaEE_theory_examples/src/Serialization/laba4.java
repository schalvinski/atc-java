package Serialization;

/**
 * Created by user on 07.04.17.
 */

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.Serializable;

public class laba4 implements Serializable {

    public static void main(String args[]) throws Exception {
        // читаем данные из файла, если файл существут то метод ready() вернет
        // true
        boolean str1;
        String s;
        StringBuffer sb = new StringBuffer();
        try {
            FileReader fr = new FileReader( "C:\\java.txt");
            // читаем данные из файла, если файл существут то метод ready()
            // вернет true
            str1 = fr.ready();
            System.out.println("Файл существует: " + str1);
            // затем заключили его в BufferedReader.
            BufferedReader br = new BufferedReader(fr);
            // Это позволило нам использовать удобный метод readLine().
            // Мы читали каждую строку до тех пор, пока они не закончились,
            // добавляя каждую строку в конец нашего StringBuffer.
            // При чтении из файла могла бы возникнуть исключительная ситуация
            // IOException, поэтому мы окружили всю нашу логику чтения файла
            // блоком try/catch.
            while ((s = br.readLine()) != null) {
                sb.append(s);
                // Буферизированная запись намного эффективнее простой записи
                // байтов по одному.После записи каждой строки (которые вручную
                // завершаем символом \n)
                sb.append("\n");
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        System.out.println(sb.toString());

        // Серилизация
        // открываем поток в файл
        try {
            FileOutputStream fos = new FileOutputStream("temp.out");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            // серилизуем туда
            oos.writeObject(sb);
            // принудительно записываем туда все данные
            oos.flush();
            // закрываем поток
            oos.close();
            fos.close();
            // создаем выходной поток
            FileInputStream fis = new FileInputStream("temp.out");
            System.out.println("сериализованный поток: ");
            // выводим на экран в байтах
            while (fis.read() != -1) {
                System.out.print((fis).read());
            }

        } catch (Exception e) {
            System.out.println("Ошибка при сериализации " + e);
        }
        System.out.println();
        // десериализация
        try {
            FileInputStream fis1 = new FileInputStream("temp.out");
            ObjectInputStream oos = new ObjectInputStream(fis1);
            StringBuffer str = (StringBuffer) oos.readObject();
            oos.close();
            fis1.close();
            System.out.println("десериализованный поток: ");
            System.out.println(str);
        } catch (Exception e) {
            System.out.println("Ошибка при десериализации " + e);
        }

    }
}